<?php
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// OPAC-integrierte Formulare
// (C) 2024 Hans-Werner Hilse <hilse@sub.uni-goettingen.de>
// Authentifizierung ggf. via PAIA-Schnittstelle
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// TODO Development (low priority):
// - Auswertung Sprachpraeferenz des Browsers
// - Auswertung/Beruecksichtigung Nutzergruppen
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
// KONFIGURATION:
// hier werden die Formulare / Texte definiert:
//
// Struktur:
//
// Array-Key gibt den Formular-Namen an, zugeordnet ist ein Array mit der Konfiguration:
//   title: Ueberschrift
//   success: Meldung nach erfolgreichem Versenden
//   mail: Konfiguration fuer zu versendende Mail, selbst ein Array mit folgenden Keys:
//      to: Adressat*innenliste
//      subject: Betreff
//      additional_headers: zusaetzliche Header fuer die Mail, zB "From". Muss ein String sein und
//                          Zeilen - auch die ggf. einzige! - mit "\r\n" abschließen
//   form: Konfiguration des eigentlichen Formulars, ein Array, das nacheinander die Formularelemente
//         spezifiziert. Die einzelnen Eintraege werden wiederum selbst in Form von Arrays definiert,
//         ihr jeweils erstes Element gibt den Typ an, weitere Parameter werden ggf. ueber assoziative
//         Keys definiert.
//
// Typen von Formularelementen:
//   'metadata': Wiedergabe der OPAC-Repraesentation (Longtitle) der Metadaten einer via PPN- oder EPN-
//               Parameter spezifizierten Publikation
//   'field':    Einfaches Texteingabefeld, Parameter: name, label, help, optional
//   'textbox':  Groesseres Texteingabefeld, Parameter: name, label, help, optional
//   'text':     Reines Info-Textelement, wird lediglich im Formular angezeigt, Parameter: text
//   'login':    Feld zum Einloggen, fuehrt eine PAIA-Authentifizierung durch
//
// Texte koennen als String angegeben werden oder als Array fuer Internationalisierung, das jeweils erste
// Element ist fuer die deutsche Sprache vorgesehen (default), ein zweites Element fuer Englisch.
// Fuer die Internationalisierung wird ein Parameter "LNG" oder alternativ "lang" ausgewertet.

$forms = array(
  'freiband' => array(
    'title'   => "Bestellung von Zeitschriftenbänden",
    'success' => "Dieser Band wurde für Sie in den Selbstabholbereich der Zentralbibliothek bestellt.",
    'mail'    => array(
      'to' => 'EMPFAENGER1@example.com, EMPFAENGER2@example.com',
      'subject' => 'Freie Bandbestellung'),
    'form'    => array(
      array('metadata'), 
      array('textbox', 'name'=>'Bandangabe', 'label'=>'Band, Erscheinungsjahr:', 'help'=>'Bitte geben Sie den gewünschten Band und - falls vorliegend - das Erscheinungsjahr an.'),
      array('login')),
  ),
  'vormbuchh' => array(
    'title'   => "Titelvormerkung",
    'success' => "Ihre Vormerkungsanfrage wurde versandt. Sie werden per E-Mail benachrichtigt, sobald der Titel f&uuml;r Sie bereitliegt.",
    'mail'    => array(
      'to' => 'EMPFAENGER1@example.com, EMPFAENGER2@example.com',
      'additional_headers' => "From: ABSENDER@example.com\r\n",
      'subject' => 'Titelvormerkung'),
    'form'    => array(
      array('metadata'), 
      array('text', 'text'=>'Dieser Titel ist beim Buchhandel bestellt bzw. wird in der SUB noch bearbeitet. Die Bereitstellung erfolgt zum n&auml;chstm&ouml;glichen Zeitpunkt. Wenn Sie diesen Titel bestellen m&ouml;chten, geben Sie bitte Ihre Bibliotheksnummer (z.B. 000705566X) und Ihr Passwort an und senden das Formular ab. Sie werden per E-Mail benachrichtigt, sobald der Titel f&uuml;r Sie bereitliegt.'), 
      array('login')),
  ),
  'digiw' => array(
    'title'   => "DigiWunschbuch - Digitalisierungsanfrage",
    'success' => "Ihre Digitalisierungsanfrage wurde versandt. Wir werden den Band sichten und Ihnen umgehend ein verbindliches Angebot unterbreiten.",
    'mail'    => array(
      'to' => 'EMPFAENGER1@example.com, EMPFAENGER2@example.com',
      'subject' => 'DigiWunschbuch - Digitalisierungsanfrage'),
    'form'    => array(
      array('metadata'), 
      array('text', 'text'=>'Mit diesem Formular starten Sie eine Anfrage nach der Digitalisierbarkeit des von Ihnen gew&uuml;nschten Buches. Wir werden den Band sichten und Ihnen umgehend ein verbindliches Angebot unterbreiten. Die Digitalisierung beginnt erst nach Ihrer endg&uuml;ltigen Zusage. Mit der Bestellung &uuml;bernehmen Sie dann eine <a href="http://digiwunschbuch.sub.uni-goettingen.de/">Buchpatenschaft</a>.'),
      array('text', 'text'=>'Bitte geben Sie Ihre pers&ouml;nlichen Daten ein:'), 
      array('text', 'text'=>'Rechnungsanschrift:'), 
      array('field', 'name'=>'Re_Name', 'label'=>'Name, Vorname'),
      array('field', 'name'=>'Re_Strasse', 'label'=>'Stra&szlig;e und Hausnummer'),
      array('field', 'name'=>'Re_Ort', 'label'=>'PLZ / Ort'),
      array('field', 'name'=>'Re_EMail', 'label'=>'E-Mail (optional)', 'optional'=>true),
      array('textbox', 'name'=>'Bemerkung', 'label'=>'Bemerkung (optional)', 'optional'=>true)),
  ),
);

// PAIA Base-URL:
$PAIA = 'https://paia.gbv.de/DE-7';

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ab hier sollten fuer die Konfiguration keine weiteren Anpassungen noetig sein
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
require('paiaauth.php');
error_reporting(0);


// Internationalisierung:
function __($text) {
  $language=0;
  if(@$_REQUEST['LNG']=='EN' || @$_REQUEST['lang']=='en') $language=1;
  if(is_array($text)) $text=$text[$language];
  return $text;
}

// Textbereinigung der OPAC-Antworten:
function tidy_opac($data) {
    return preg_replace(array(
      '/<br \/>\n/s',
      '/&lt;/','/&gt;/','/&quot;/','/&apos;/','/&amp;/',
      '/<TR>/',
      '/ <div class="loader" ><\/div>  /',
      '/<[aA][^>]*>(.*?)<\/[aA]>/',
  ), array(
      '',
      '<','>','"',"'",'&',
      '<TR><TH scope="row">',
      '',
      '$1'
  ), $data);
}

// Kurztitel und ggf. PPN ermitteln
function fetch_shorttitle($ppn, $epn=false) {
  $search="IKT=12&TRM=$ppn";
  if(!$ppn) {
    if(!$epn) return array(false);
    $search="IKT=8000&TRM=$epn";
  }
  preg_match('/.*<SHORTTITLE.*?PPN="([^"]+)".*?>(.*)<\/SHORTTITLE>.*/s', file_get_contents("http://$_SERVER[SERVER_NAME]:8080/DB=1/LNG=DU/XML=1.0/CMD?ACT=SRCHA&CHARSET=UTF-8&$search"), $data);
  return array(true, 'ppn'=>@$data[1], 'shorttitle'=>tidy_opac(@$data[2]));
}

// Langtitel aus dem Katalog lesen
function fetch_metadata($ppn, $epn=false) {
  $ppn = $ppn ? $ppn : fetch_shorttitle(false, $epn)['ppn'];
  if(!$ppn) return array(false, "could not resolve EPN");

  // Mittels PPN Titel von OPAC anfordern
  $data=file_get_contents("http://$_SERVER[SERVER_NAME]:8080/DB=1/LNG=DU/XML=1.0/PPN?CHARSET=UTF-8&PPN=" . $ppn, "r");
  if(!$data) return array(false, "could not open XML Fulltitle input");

  // Ausmisten und ueberarbeiten
  return array(true, tidy_opac(preg_replace('/.*<LONGTITLE[^>]*>(.*)<\/LONGTITLE>.*/s', '$1', $data)));
}

// Vorbereitung: In diesem Array werden Fehlermeldungen akkumuliert, deren
// Vorhandensein dafuer sorgt, dass das Formular nicht abgeschickt, sondern
// mitsamt den Meldungen erneut angezeigt wird.
$errors = array();

// Im Parameter "F" wird der Name des anzuzeigenden Formulars spezifiziert.
$form = @$forms[@$_REQUEST['F']];
if(!$form) {
  $form=array();
  $errors['general'] = __('Interner Fehler: Formular nicht gefunden.', 'Internal error: form not found.');
}

// erste Pruefung externer Parameter, wo noetig
foreach($form['form'] as $item) {
  if($item[0]=='metadata') {
    // Pruefung auf gueltige EPN/PPN
    list($ok,$data) = fetch_metadata($_REQUEST['PPN'], $_REQUEST['EPN']); 
    if($ok) {
      $form['metadata']=$data;
    } else {
      $errors['general'] = __('Interner Fehler: EPN/PPN nicht gefunden.', 'Internal error: EPN/PPN not found.');
    }
  }
}

// Das Formular wurde abgeschickt. Wir pruefen die eingegangenen Werte auf Gueltigkeit:
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  foreach($form['form'] as $n=>$item) {
    switch($item[0]) {
    case 'login':
      if(!isset($_REQUEST['login_user'])) {
        $errors['login'] = array(
          'Bitte geben Sie Ihre Bibliotheksnummer ein.',
          'Please enter your library account number.');
        break;
      }
      if(!@$_REQUEST['login_password']) {
        $errors['login'] = array(
          'Bitte geben Sie das Passwort Ihres Bibliothekskontos ein.',
          'Please enter your library account password.');
        break;
      }
      list($error, $data, $msg) = paiaauth($PAIA, $_REQUEST['login_user'], $_REQUEST['login_password']);
      if($error=="ERR_AUTH") {
        $errors['login'] = array(
          'Die Bibliotheksnummer und/oder das Passwort sind falsch.',
          'The entered library account number and/or the password are wrong.');
      } elseif($error=="ERR_ALLOWED") {
        $errors['login'] = array(
          'Keine Berechtigung.',
          'Not allowed.');
      } elseif($error) {
        $errors['login'] = array(
          'Interner Fehler bei der Authentifizierung.',
          'Internal error during authentification process.');
      }
      $form['login']=$data;
      $form['login']['user']=$_REQUEST['login_user'];
      break;
    case 'field':
    case 'textbox':
      if(!@$item['optional'] && !@$_REQUEST[$item['name']]) {
        $errors[$item['name']] = array(
          'Bitte machen Sie eine Angabe.',
          'Please specify.');
      }
      break;
    }
  }
  if(!count($errors)) {
    // keine Fehler gefunden
    if($form['mail']) {
      // Formular sieht Mailversand vor - also wird eine Mail gebaut und verschickt
      $txt = "";
      $replyto = trim($form['login']['email']);
      foreach($form['form'] as $item) {
        switch($item[0]) {
        case 'login':
          $txt .= 'Bibliotheksnummer: '.trim($form['login']['user'])."\r\n";
          $txt .= 'Name: '.trim($form['login']['name'])."\r\n";
          $txt .= 'Email: '.$replyto."\r\n\r\n";
          break;
	case 'field':
        case 'textbox':
          $txt .= $item['name'] . ": ".$_REQUEST[$item['name']]."\r\n\r\n";
          break;
        case 'metadata':
          $txt .= $_REQUEST['EPN']?"EPN: ".$_REQUEST['EPN']."\r\n":'';
          $title = fetch_shorttitle($_REQUEST['PPN'], $_REQUEST['EPN']);
          $txt .= "PPN: ".$title['ppn']."\r\n";
          $txt .= "Titel:\r\n".$title['shorttitle']."\r\n\r\n";
          break;
        }
      }
      if(!mail($form['mail']['to'], $form['mail']['subject'] ? $form['mail']['subject'] : $form['title'], $txt,
        "Content-Type: text/plain; charset=UTF-8\r\n".
        @$form['mail']['additional_headers'].
        ($replyto ? "Reply-To: $replyto\r\n" : ''))) {
        $errors['general'] = array(
          'Es ist ein interner Fehler aufgetreten. Bitte versuchen Sie es zu einem späteren Zeitpunkt noch einmal.',
          'There has been an internal error. Please try again later.');
      }
    }
  }
  if(!count($errors)) {
    // Erfolgreich, umleiten auf uns selbst fuer Erfolgsmeldung
    $q = '?F='.urlencode($_REQUEST['F']).'&success=1';
    if($_REQUEST['EPN']) $q .= '&EPN='.urlencode($_REQUEST['EPN']);
    if($_REQUEST['PPN']) $q .= '&PPN='.urlencode($_REQUEST['PPN']);
    header('Location: '.$_SERVER['PHP_SELF'].$q);
    die();
  }
}

// in allen anderen Fällen wird jetzt das Formular gebaut und ausgegeben, ggf. ergänzt um Fehlermeldungen

header('Content-Type: text/html; charset=UTF-8');
?>
<html>
  <head>
    <title><?php echo $form['title']; ?></title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <style type="text/css">
      div.metadata { margin: 0 35px; box-sizing: border-box; overflow-y: scroll; max-height: 60%; background: #eee;}
      div.metadata table { padding: 1em; }
      table th { vertical-align: top; padding-right: 1em;}
      table tr { padding: 0.5em;}
    </style>
  </head>
  <body>
    <div class="container">
      <h1><?php echo __($form['title']); ?></h1>
<?php if($_REQUEST['success']) { ?>
      <div class="mb-3 alert"><?php echo __($form['success']); ?></div>
      <button type="button" class="btn btn-primary" onclick="window.close()"><?php echo __(array('Fenster/Tab schlie&szlig;en', 'Close window/tab')); ?></button> 
<?php } else if($errors['general']) { ?>
      <div class="mb-3 alert alert-danger"><?php echo __($errors['general']); ?></div>
<?php } else {
      // build the form
?>
      <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
        <input type="hidden" name="F" value="<?php echo $_REQUEST['F']; ?>" >
<?php if($_REQUEST['EPN']) { ?>
        <input type="hidden" name="EPN" value="<?php echo $_REQUEST['EPN']; ?>" >
<?php } if($_REQUEST['PPN']) { ?>
        <input type="hidden" name="PPN" value="<?php echo $_REQUEST['PPN']; ?>" >
<?php }
  foreach($form['form'] as $item) {
    switch($item[0]) {
    case 'metadata':
?>
        <div class="mb-3 metadata"><table><?php echo $form['metadata']; ?></table></div>
<?php
      break;
    case 'text':
?>
        <div class="mb-3"><?php echo __($item['text']); ?></div>
<?php
      break;
    case 'textbox':
?>
        <div class="mb-3">
          <label for="<?php echo $item['name']; ?>" class="form-label"><?php echo __($item['label']); ?></label>
<?php
      if($errors[$item['name']]) {
?>
          <div class="mb-3 alert alert-danger"><?php echo __($errors[$item['name']]); ?></div>
<?php
      }
?>
          <textarea name="<?php echo $item['name']; ?>" id="<?php echo $item['name']; ?>" class="form-control" aria-describedby="<?php $item['name']; ?>_help"><?php echo htmlentities($_REQUEST[$item['name']]) ?></textarea>
          <div id="<?php $item['name']; ?>_help" class="form-text"><?php echo __($item['help']); ?></div>
        </div>
<?php
      break;
    case 'field':
?>
        <div class="mb-3">
          <label for="<?php echo $item['name']; ?>" class="form-label"><?php echo __($item['label']); ?></label>
<?php
      if($errors[$item['name']]) {
?>
          <div class="mb-3 alert alert-danger"><?php echo __($errors[$item['name']]); ?></div>
<?php
      }
?>
          <input type="text" name="<?php echo $item['name']; ?>" id="<?php echo $item['name']; ?>" class="form-control" aria-describedby="<?php $item['name']; ?>_help" value="<?php echo htmlentities($_REQUEST[$item['name']]) ?>" />
          <div id="<?php $item['name']; ?>_help" class="form-text"><?php echo __($item['help']); ?></div>
        </div>
<?php
      break;
    case 'login':
?>
        <div class="mb-3">
          <label for="login_user" class="form-label"><?php echo __(array('Bibliotheksnummer:','User number:')); ?></label>
          <input type="text" name="login_user" id="login_user" class="form-control" value="<?php echo htmlentities($_REQUEST['login_user']) ?>">
          <label for="login_password" class="form-label"><?php echo __(array('Passwort:','Password:')); ?></label>
          <input type="password" name="login_password" id="login_password" class="form-control" value="">
        </div>
<?php
      if($errors['login']) {
?>
        <div class="mb-3 alert alert-danger"><?php echo __($errors['login']); ?></div>
<?php
      }
      break;
    }
  }
?>
        <div class="mb-3">
        <input type="submit" class="form-control" value="<?php echo __($form['submit'] ? $form['submit'] : array('Abschicken', 'Submit request')); ?>">
        </div>
      </form>
<?php
}
?>
    </div>
  </body>
</html>
