<?php
/*
* A very simple PAIA auth client
* (C)2024 by Hans-Werner Hilse <hilse@sub.uni-goettingen.de>
*
* usage example:
*
*   list($error, $data, $msg) = paiaauth('https://paia.gbv.de/MYBIB', $_REQUEST['username'], $_REQUEST['password']);
*   if(!$error) {
*    ...
*   }
*
* remember that allow_url_fopen PHP config must allow access to PAIA service
*
* TODO:
*  - separate baseURLs for PAIA core and PAIA auth?
*  - implement data checking only for a given access_token 
*
* 2024-04-17 HWH: initial version
* 2024-05-07 HWH: cleanup, documentation
*/

/* perform a PAIA authentification
*
* return value is an array (or rather a list), of which the first element
* indicates an error (string value) or is "false" in case of success (!)
* second element returns context information, like the patron data in case
* of authentication success (also when validation steps fail, so this does
* NOT indicate an athentication success!). Third element might contain a
* human readable error string.
*
* arguments:
*   - PAIA Base URL
*   - the username to be authenticated
*   - the according password
*   - an array of allowed user types - or false when those are not to be validated
*   - a boolean indicating if expired accounts should be allowed
*   - an array listing valid user states
*
* see PAIA documentation (and the actual replys of your interface) for sensible
* values.
*/ 
function paiaauth(
  $paia_base_url,
  $username,
  $password,
  $allow_types=false,
  $allow_expired=false,
  $allow_status=array(0)
) {
  // our default set of stream options
  // see PHP stream documentation for details/other options
  $stream_default_options = array(
    'socket' => array(
      'tcp_nodelay' => true),
    'ssl' => array(
      'disable_compression' => true),
    'http' => array(
      'user_agent' => 'paiaauth.php/1.0',
      'ignore_errors' => true,
      'header' => array('a'=>'Accept: application/json', 'b'=>'Connection: close'),
      'timeout' => 2.0,
      'protocol_version' => 1.1,
      'max_redirects' => 2,
    )); 

  $login_data = http_build_query(array(
    'grant_type' => 'password',
    'username' => $username,
    'password' => $password,
    'scope' => 'read_patron'));

  $login_url = $paia_base_url . '/auth/login';

  $ch=false; $reply=false;

  if(extension_loaded('curl')) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, 'paiaauth.php/1.0');
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
    curl_setopt($ch, CURLOPT_TIMEOUT, 2);
    curl_setopt($ch, CURLOPT_URL, $login_url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $login_data);
    $reply = curl_exec($ch);
    curl_close($ch);
    if(!$reply)
      return array('ERR_HTTP', null, 'transport layer error when connecting to PAIA server for login: ' . curl_error($ch));
  } else {
    // try PAIA auth /login
    $ctx = stream_context_create(array_replace_recursive($stream_default_options, array('http' => array(
      'method' => 'POST',
      'header' => array('Content-type: application/x-www-form-urlencoded;charset=UTF-8'),
      'content' => $login_data
    ))));

    $reply = file_get_contents($login_url, false, $ctx);
    if(!$reply)
      return array('ERR_HTTP', null, 'transport layer error when connecting to PAIA server for login');
  }

  // decode answer
  $login = json_decode($reply, true);
  if(!$login)
    return array('ERR_JSON', $reply, 'malformed json returned when logging in');
  
  // check for PAIA protocol level error
  if(isset($login['error']))
    return array('ERR_AUTH', $login, 'login error');
  
  // check for mandatory return values
  if(!isset($login['patron']) || !isset($login['access_token']))
    return array('ERR_PAIA', $login, 'no patron id or no access token returned');
  
  // check if we are allowed to read patron data
  // we consider it as an error if we are not allowed, even if the login itself
  // was technically successful
  if(!isset($login['scope']) || $login['scope']!='read_patron')
    return array('ERR_PAIA', $login, 'no access to patron scope granted');
  
  // now do the second request, this time for patron information
  $patron_url = $paia_base_url . '/core/' . rawurlencode($login['patron']);
  $auth_header = array('Accept: application/json', 'Authorization: Bearer ' . $login['access_token']);
  if(extension_loaded('curl')) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, 'paiaauth.php/1.0');
    curl_setopt($ch, CURLOPT_URL, $patron_url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $auth_header);
    curl_setopt($ch, CURLOPT_HTTPGET, true);
    $reply = curl_exec($ch);
    curl_close($ch);
    if(!$reply)
      return array('ERR_HTTP', null, 'transport layer error when connecting to PAIA server for login: ' . curl_error($ch));
  } else {
    $ctx = stream_context_create(array_replace_recursive($stream_default_options, array('http'=>array(
      'method'=>'GET',
      'header'=>$auth_header,
    ))));

    $reply = file_get_contents($patron_url, false, $ctx);
    if(!$reply)
      return array('ERR_HTTP', null, 'transport layer error when connecting to PAIA server for patron info');
  }
  
  // decode answer
  $patron = json_decode($reply, true);
  if(!$patron)
    return array('ERR_JSON', $reply, 'malformed json returned when getting patron info');

  // check for PAIA protocol level error  
  if(isset($patron['error']))
    return array('ERR_PATRON', $patron, 'error reading patron info');
  
  // status from PAIA is optional. We'll only allow a missing one when $allow_status==array().
  if(!isset($patron['status']))
    if(count($allow_status) > 0)
      return array('ERR_ALLOWED', $patron, 'patron info does not contain status information');

  // otherwise, it must be a value in $allow_status array
  if(!in_array($patron['status'], $allow_status))
    return array('ERR_ALLOWED', $patron, 'patron status is not allowed: ' . $patron['status']);

  // default expiry behaviour is to NOT CHECK the expiry date
  // assumption is that PAIA patron status 2 is used for this (check by PAIA server)
  if($allow_expired) {
    if(!isset($patron['expires'])) {
      // a missing expiry date is allowed by PAIA. we only allow this case when the
      // $allow_expired array contains the key 'no_expiry'
      if(!isset($allow_expired['no_expiry']))
        return array('ERR_ALLOWED', $patron, 'patron info does not contain an expiry date');
    } else {
      // if PAIA data has an expiry date, check it against $allow_expired['after']
      $expires = date_create($patron['expires']);
      if(isset($allow_expired['after']) && $expires <= $allow_expired['after'])
        return array('ERR_ALLOWED', $patron, 'patron account is expired');
    }
  }

  // if successful auth is restricted to patrons of specific types, they are checked
  if(is_array($allow_types)) {
    if(!isset($patron['type']))
      return array('ERR_ALLOWED', $patron, 'patron info does not contain any types');
    $allow = false;
    foreach($allow_types as $type)
      if(in_array($type, $patron['type']))
        $allow = true;
    if(!$allow)
      return array('ERR_ALLOWED', $patron, 'patron types are not in allowed set');
  }

  // success
  return array(false, $patron, null);
}

